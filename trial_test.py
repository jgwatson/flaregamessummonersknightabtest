'''
Created on Feb 9, 2017

@author: John G Watson
'''

from groupdata import GroupData, plot_histogram, plot_histogram_binomial
from scipy.stats import mannwhitneyu
from scipy.stats import fisher_exact
from scipy.stats import chi2_contingency

import numpy as np

def main():
    # Read data from file
    test_data = GroupData(file_name="TestTask.csv", group_column=1, skip=[0], col_separator=";")
    
    # Plot Revenue_30_days_after_installation
    plot_histogram(test_data.groups, test_data.grouped_features, 0, "Descriptive statistics of\nRevenue_30_days_after_installation", "Revenue_30_days_after_installation.jpg")
    
    # Plot Adjusted Revenue_30_days_after_installation
    plot_histogram(test_data.groups, test_data.grouped_features, 0, "Descriptive statistics of adjusted \nRevenue_30_days_after_installation", "Adjusted_Revenue_30_days_after_installation.jpg", skip=[0], percentiles=[5, 95], use_blocks=False)
    
    # plot binary plots
    for var_index in range(1, 5):
        var_name = test_data.get_feature_name(var_index)
        title = "Descriptive statistics of\n" + var_name
        file_name = var_name + ".jpg"
        plot_histogram_binomial(test_data.groups, test_data.grouped_features, var_index, title, file_name)
    
    #Perform tests
    var0_u_value, var0_p_value = test_mannwhitneyu(test_data.groups, test_data.grouped_features, 0)
    print("Mann-Whitney U test of " + test_data.get_feature_name(0) + "is:\n")
    print("U: " + str(var0_u_value) + "\n")
    print("p-value: " + str(var0_p_value) + "\n")
    
    for var_index in range(1, 2):
        var_name = test_data.get_feature_name(var_index)
        varf, pf = test_fisher_binomial(test_data.groups, test_data.grouped_features, var_index)
        print("Fisher test for variable " + var_name + "\n")
        print("Value: " + str(varf) + "\n") 
        print("p-value: " + "{0:.10f}".format(pf) + "\n") 
        varf, pf = test_chi_square_binomial(test_data.groups, test_data.grouped_features, var_index)
        print("Chi-squared test for variable " + var_name + "\n")
        print("Value: " + str(varf) + "\n") 
        print("p-value: " + "{0:.10f}".format(pf) + "\n") 

    
def test_mannwhitneyu(groups, features, variable_index):
    u_value, p_value = mannwhitneyu(np.array(features["b"][variable_index]), np.array(features["a"][variable_index]),alternative="greater")
    return u_value, p_value

def test_fisher_binomial(groups, features, variable_index):
    values = create_contingency(features, variable_index)
    value, pvalue = fisher_exact(values)
    return value, pvalue

def test_chi_square_binomial(groups, features, variable_index):
    values = create_contingency(features, variable_index)
    value, pvalue, dof, ex = chi2_contingency(values)
    return value, pvalue

def create_contingency(features, variable_index):
    count_a_0 = len([i for i in features["a"][variable_index] if i == 0])
    count_a_1 = len([i for i in features["a"][variable_index] if i == 1])
    count_b_0 = len([i for i in features["b"][variable_index] if i == 0])
    count_b_1 = len([i for i in features["b"][variable_index] if i == 1])
    return [[count_a_0, count_a_1], [count_b_0, count_b_1]]

        
if __name__ == '__main__':
    main()
