'''
Created on Feb 10, 2017

@author: John G Watson
'''

import csv
import math
import re
from astroML.density_estimation import bayesian_blocks
import matplotlib.pyplot as plt
import numpy as np


def plot_histogram(groups, features, variable_index, title, file_name, skip=[], percentiles=[0,100], use_blocks=False):
    fig = plt.figure(figsize=(5,5 * len(groups)))
    fig.suptitle(title, fontsize=12)
    
    axis = []
    std_f = []
    max_f = []
    min_f = []
    mean_f = []
    median_f = []
    count_f = []
    
    for group_index, group in enumerate(groups):
        if skip == []:
            feature = features[group][variable_index]
        else:
            feature = [j for j in features[group][variable_index] if j not in skip]
        if group == "Total":
            axis.append(fig.add_subplot(len(groups), 1,  group_index + 1))
            bins = bayesian_blocks(feature)
            max_x = np.percentile(feature, percentiles[1])
            min_x = np.percentile(feature, percentiles[0])
        else:
            axis.append(fig.add_subplot(len(groups), 1, group_index + 1, sharex=axis[0]))
        
        std_f.append(np.std(feature))
        mean_f.append(np.mean(feature))
        median_f.append(np.median(feature))
        max_f.append(max(feature))
        min_f.append(min(feature))
        count_f.append(len(feature))
        
        text_string = "STD: " + "{0:.2f}".format(std_f[group_index])
        text_string += "\n" + "Mean: " + "{0:.2f}".format(mean_f[group_index])
        text_string += "\n" + "Median: " + "{0:.2f}".format(median_f[group_index])
        text_string += "\n" + "Max: " + "{0:.2f}".format(max_f[group_index])
        text_string += "\n" + "Min: " + "{0:.2f}".format(min_f[group_index])
        text_string += "\n" + "Count: " + str(count_f[group_index])
        
        
        axis[group_index].annotate(text_string, xy=(1, 1), xycoords='axes fraction', fontsize=7,
                xytext=(-5, -5), textcoords='offset points',
                ha='right', va='top')
        
        if use_blocks == False:
            bins = int(math.sqrt(len(feature)))

        plt.hist(feature, bins, histtype="stepfilled", color = "firebrick", alpha=0.5, edgecolor="none", normed=False)
        boxplt = axis[group_index].twinx()
        b = boxplt.boxplot(feature, vert=False, showmeans = True)
        axis[group_index].set_title(group, fontsize=10)
        plt.setp(b["boxes"], color="darkred")
        plt.setp(b["whiskers"], color="sandybrown")
        plt.setp(b["means"], markeredgecolor="none", markerfacecolor="darkred")
        plt.setp(b["fliers"], markeredgecolor="peachpuff")
        plt.setp(b["caps"], color ="sandybrown")
        plt.setp(b["medians"], color="darkred")
        plt.setp(axis[group_index].get_xticklabels(), visible=False, fontsize=8)
        plt.setp(axis[group_index].get_yticklabels() , fontsize=8)
        plt.setp(boxplt.get_yticklabels(), visible=False)
        plt.setp(boxplt.get_xticklabels(), visible=False)
    
    
    plt.xlim([min_x,max_x])
    plt.setp(axis[len(groups)-1].get_xticklabels(), visible=True)

    plt.savefig(file_name)
    
def plot_histogram_binomial(groups, features, variable_index, title, file_name):
    fig = plt.figure(figsize=(5,5 * len(groups)))
    fig.suptitle(title, fontsize=12)
    
    axis = []
    per0_f = []
    per1_f = []
    mean_f = []
    count_f = []
    
    for group_index, group in enumerate(groups):
 
        feature = features[group][variable_index]
        if group == "Total":
            axis.append(fig.add_subplot(len(groups), 1, group_index + 1))
        else:
            axis.append(fig.add_subplot(len(groups), 1, group_index + 1, sharex=axis[0]))
        
        per0_f.append(len([i for i in feature if i == 0]) / len(feature))
        per1_f.append(len([i for i in feature if i == 1]) / len(feature))
        mean_f.append(np.mean(feature))
        count_f.append(len(feature))

        text_string = "0s: " + "{0:.2%}".format(per0_f[group_index])
        text_string += "\n" + "1s: " + "{0:.2%}".format(per1_f[group_index])
        text_string += "\n" + "Mean: " + "{0:.2f}".format(mean_f[group_index])
        text_string += "\n" + "Count: " + str(count_f[group_index])
        
        axis[group_index].annotate(text_string, xy=(1, 1), xycoords='axes fraction', fontsize=7,
                xytext=(-5, -5), textcoords='offset points',
                ha='right', va='top')
        
        plt.hist(feature, 2, histtype="stepfilled", color = "firebrick", alpha=0.5, edgecolor="none", normed=False)
        axis[group_index].set_title(group, fontsize=10)
        plt.setp(axis[group_index].get_xticklabels(), visible=False, fontsize=8)
        plt.setp(axis[group_index].get_yticklabels(), fontsize=8)
        
    plt.setp(axis[len(groups)-1].get_xticklabels(), visible=True)
    plt.savefig(file_name)
    
class GroupData(object):
    def __init__(self, file_name, group_column=0, skip=[], col_separator=",", contains_headers=True, max_row_scan=2000):
        self.group_vector = []
        self.features = []
        self.column_names = []
        self.group_name = ""
        self.grouped_features = {}
        
        with open(file_name, "r") as data_file:
            GroupData._read_data(self, data_file, group_column, skip, col_separator, contains_headers, max_row_scan)
        self.features = list(zip(*self.features))
        self.groups = list(set(self.group_vector))
        self.groups.insert(0, "Total")
        self.grouped_indexes, self.grouped_features = GroupData._grouped_features(self, self.group_vector, self.features)  
        
    def _read_data(self, csv_file, group_column, skip, col_separator, contains_headers, max_row_scan):
        is_first_line = True
        csv_reader = csv.reader(csv_file, delimiter=col_separator)
        non_features = skip
        non_features.append(group_column)
        col_types = GroupData._detect_column_type(self, csv_file, col_separator, contains_headers, non_features, max_row_scan)
        
        for row in csv_reader:
            if contains_headers and is_first_line:
                self.column_names = [i for j, i in enumerate(row) if j not in non_features]
                self.group_name = row[group_column]
                is_first_line = False
            elif is_first_line:
                column_names = ["Column" + i for i in enumerate(row)]
                self.column_names = [i for j, i in enumerate(column_names) if j not in non_features]
                self.group_name = "Group"
            else:
                feature = [i for j, i in enumerate(row) if j not in non_features]
                for col, var in enumerate(feature):
                    if col_types[col] == str:
                        feature[col] = str(var)
                        continue
                    if col_types[col] == int:
                        feature[col] = int(var)
                        continue
                    if col_types[col] == float:
                        feature[col] = float(var)
                        continue
                group = row[group_column]
                self.features.append(feature)
                self.group_vector.append(group)   
    
    def _detect_column_type(self, csv_file, col_separator, contains_headers, non_features, max_row_scan):
        with open(csv_file.name) as file_for_headers:
            csv_reader = csv.reader(file_for_headers, delimiter=col_separator)
            data_type_regex = {int: re.compile(r'^-?\d+$'), float: re.compile(r'^\d+\.{1}\d+$'), str: re.compile(r'[^\d\.-]')}
            column_types = []
                
            for row_number, row in enumerate(csv_reader):
                feature = [i for j, i in enumerate(row) if j not in non_features]
                if column_types == []:
                    column_types = [None] * len(feature)
                if contains_headers and row_number == 0:
                    continue
                elif row_number > max_row_scan:
                    return column_types
                else:
                    for cell_index, cell in enumerate(feature):
                        if column_types[cell_index] != str:
                            for _type, pattern in data_type_regex.items():
                                if re.search(pattern, cell):
                                    if _type == int and column_types[cell_index] == float:
                                        continue
                                    column_types[cell_index] = _type
                                    break
                                
    def _grouped_features(self, group_vector, features):
        grouped_features = {}
        grouped_indexes = {}
        for group in self.groups:
            if group == "Total":
                grouped_features["Total"] = features
            else:
                grouped_indexes[group] = [i for i,v in enumerate(self.group_vector) if v == group]
                group_features = []
                for feature in features:
                    group_features.append([feature[k] for k in grouped_indexes[group]])
                grouped_features[group] = group_features
        return grouped_indexes, grouped_features
    
    def get_feature_name(self, index):
        for i, name in enumerate(self.column_names):
            if i == index:
                return name  
    
            